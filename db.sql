create database users;

use users;

create table users(
    id int(11) not null auto_increment,
    user varchar(20) not null,
    password varchar(120) not null,
    name varchar(120) not null,
    PRIMARY KEY (id)
);

create table access_users(
    id int(11) not null auto_increment,
    id_user int(11) not null,
    access_date timestamp not null DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY (id),
    FOREIGN KEY (id_user) REFERENCES users(id)
);

alter table access_users add primary key (id);
alter table access_users modify id int(11) not null auto_increment;

insert into users (user,password,name) values ('user_1','QWERTY','name_1');
insert into users (user,password,name) values ('user_2','QWERTY','name_2');
insert into users (user,password,name) values ('user_3','QWERTY','name_3');
insert into users (user,password,name) values ('user_4','QWERTY','name_4');

insert into access_users (id_user) select id from users where user='user_1';
insert into access_users (id_user) select id from users where user='user_1';
insert into access_users (id_user) select id from users where user='user_2';
insert into access_users (id_user) select id from users where user='user_3';
insert into access_users (id_user) select id from users where user='user_1';
insert into access_users (id_user) select id from users where user='user_1';
insert into access_users (id_user) select id from users where user='user_4';
insert into access_users (id_user) select id from users where user='user_1';
insert into access_users (id_user) select id from users where user='user_2';
insert into access_users (id_user) select id from users where user='user_2';
insert into access_users (id_user) select id from users where user='user_2';
insert into access_users (id_user) select id from users where user='user_4';
insert into access_users (id_user) select id from users where user='user_3';
insert into access_users (id_user) select id from users where user='user_1';

delete from access_users;
delete from users;

drop table access_users;
drop table users;

create table users_copy(
    id int(11) not null auto_increment,
    user varchar(20) not null,
    password varchar(120) not null,
    name varchar(120) not null,
    PRIMARY KEY (id)
);

create table access_users_copy(
    id int(11) not null auto_increment,
    id_user int(11) not null,
    access_date timestamp not null,
    PRIMARY KEY (id),
    FOREIGN KEY (id_user) REFERENCES users_copy(id)
);

delete from access_users_copy;
delete from users_copy;

drop table access_users_copy;
drop table users_copy;

